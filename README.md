# Minimal Fennel with Raylib
This project is a minimal setup required to run Raylib using Fennel. 

## What's required
Dependencies required to run this example.

### Windows
 - LuaJIT (tested with OpenRestys https://openresty.org/en/ and LuaPower https://luapower.com/).
 - Compiled Fennel script https://fennel-lang.org/downloads/fennel-1.1.0
 - Raylib for Lua

- Place inside `/lib/`
  - `raylib.lua`

- Place inside `/bin/`
  - `fennel` (compiled script)
  - `libraylib64.dll`
  - `lua51.dll`
  - `luajit.exe` (OpenResty or LuaPower)

### Linux
Tested with Manjaro Linux
 - Install LuaJIT
 - Install Raylib
 - Compiled Fennel script https://fennel-lang.org/downloads/fennel-1.1.0 (optional)

You can run it with `fennel --lua luajit game.fnl` if you wish

## Additionals
I included `struct.fnl` converted from https://github.com/iryont/lua-struct because i needed to read some binary data correctly