(local rl (require :lib/raylib))
(local struc (require :lib/struct))

(local screen-width 1280)
(local screen-height 720)

(rl.InitWindow screen-width screen-height "Minimal Raylib")
(rl.SetTargetFPS 60)

(var color rl.WHITE)

(while (not (rl.WindowShouldClose))
  (if (rl.IsCursorOnScreen)
    (rl.ClearBackground rl.RED)
    (rl.ClearBackground rl.WHITE))

  (rl.BeginDrawing)
  (rl.EndDrawing))

(rl.CloseWindow)