(local unpack (or table.unpack _G.unpack))
(local struct {})
(set struct.license "
 * Copyright (c) 2015-2020 Iryont <https://github.com/iryont/lua-struct>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.")

(set struct.help "
what is it for?

You can use it to pack and unpack binary data in pure lua. The idea is very similar to PHP unpack and pack functions.
byte order

You can use < or > at the beginning of the format string to specify the byte order. Default is little endian (<), but you can change it to big endian (>) as well. It is possible to dynamically change the byte order within the format string, so in general you can save types in different byte orders.
available types

'b' a signed char.
'B' an unsigned char.
'h' a signed short (2 bytes).
'H' an unsigned short (2 bytes).
'i' a signed int (4 bytes).
'I' an unsigned int (4 bytes).
'l' a signed long (8 bytes).
'L' an unsigned long (8 bytes).
'f' a float (4 bytes).
'd' a double (8 bytes).
's' a zero-terminated string.
'cn' a sequence of exactly n chars corresponding to a single Lua string (if n <= 0 then for packing - the string length is taken, unpacking - the number value of the previous unpacked value which is not returned).

how to use it?

local struct = require \"struct\"

local packed = struct.pack('<LIhBsbfd', 123456789123456789, 123456789, -3200, 255, 'Test message', -1, 1.56789, 1.56789)
local L, I, h, B, s, b, f, d = struct.unpack('<LIhBsbfd', packed)
print(L, I, h, B, s, b, f, d)

1.2345678912346e+017    123456789    -3200    255    Test message    -1    1.5678899288177    1.56789")


(fn struct.pack [format ...]
  (let [stream {}
        vars {1 ...}]
    (var endianness true)
    (for [i 1 (format:len) 1]
      (local opt (format:sub i i))
      (if (= opt "<") (set endianness true) (= opt ">") (set endianness false)
          (opt:find "[bBhHiIlL]")
          (let [n (or (or (or (and (opt:find "[hH]") 2)
                              (and (opt:find "[iI]") 4))
                          (and (opt:find "[lL]") 8)) 1)]
            (var val (tonumber (table.remove vars 1)))
            (local bytes {})
            (for [j 1 n 1]
              (table.insert bytes (string.char (% val (^ 2 8))))
              (set val (math.floor (/ val (^ 2 8)))))
            (if (not endianness)
                (table.insert stream (string.reverse (table.concat bytes)))
                (table.insert stream (table.concat bytes))))
          (opt:find "[fd]")
          (do
            (var val (tonumber (table.remove vars 1)))
            (var sign 0)
            (when (< val 0)
              (set sign 1)
              (set val (- val)))
            (var (mantissa exponent) (math.frexp val))
            (if (= val 0) (do
                            (set mantissa 0)
                            (set exponent 0))
                (do
                  (set mantissa
                       (* (- (* mantissa 2) 1)
                          (math.ldexp 0.5 (or (and (= opt :d) 53) 24))))
                  (set exponent (+ exponent (or (and (= opt :d) 1022) 126)))))
            (local bytes {})
            (if (= opt :d)
                (do
                  (set val mantissa)
                  (for [i 1 6 1]
                    (table.insert bytes
                                  (string.char (% (math.floor val) (^ 2 8))))
                    (set val (math.floor (/ val (^ 2 8))))))
                (do
                  (table.insert bytes
                                (string.char (% (math.floor mantissa) (^ 2 8))))
                  (set val (math.floor (/ mantissa (^ 2 8))))
                  (table.insert bytes
                                (string.char (% (math.floor val) (^ 2 8))))
                  (set val (math.floor (/ val (^ 2 8))))))
            (table.insert bytes
                          (string.char (% (math.floor (+ (* exponent
                                                            (or (and (= opt :d)
                                                                     16)
                                                                128))
                                                         val))
                                          (^ 2 8))))
            (set val (math.floor (/ (+ (* exponent (or (and (= opt :d) 16) 128))
                                       val)
                                    (^ 2 8))))
            (table.insert bytes
                          (string.char (% (math.floor (+ (* sign 128) val))
                                          (^ 2 8))))
            (set val (math.floor (/ (+ (* sign 128) val) (^ 2 8))))
            (if (not endianness)
                (table.insert stream (string.reverse (table.concat bytes)))
                (table.insert stream (table.concat bytes))))
          (= opt :s)
          (do
            (table.insert stream (tostring (table.remove vars 1)))
            (table.insert stream (string.char 0))) (= opt :c)
          (let [n (: (format:sub (+ i 1)) :match "%d+")]
            (var str (tostring (table.remove vars 1)))
            (var len (tonumber n))
            (when (<= len 0)
              (set len (str:len)))
            (when (> (- len (str:len)) 0)
              (set str (.. str (string.rep " " (- len (str:len))))))
            (table.insert stream (str:sub 1 len))
            (set-forcibly! i (+ i (n:len))))))
    (table.concat stream)))
(fn struct.unpack [format stream pos]
  (let [vars {}]
    (var iterator (or pos 1))
    (var endianness true)
    (for [i 1 (format:len) 1]
      (local opt (format:sub i i))
      (if (= opt "<") (set endianness true) (= opt ">") (set endianness false)
          (opt:find "[bBhHiIlL]")
          (let [n (or (or (or (and (opt:find "[hH]") 2)
                              (and (opt:find "[iI]") 4))
                          (and (opt:find "[lL]") 8)) 1)
                signed (= (opt:lower) opt)]
            (var val 0)
            (for [j 1 n 1]
              (local byte (string.byte (stream:sub iterator iterator)))
              (if endianness (set val (+ val (* byte (^ 2 (* (- j 1) 8)))))
                  (set val (+ val (* byte (^ 2 (* (- n j) 8))))))
              (set iterator (+ iterator 1)))
            (when (and signed (>= val (^ 2 (- (* n 8) 1))))
              (set val (- val (^ 2 (* n 8)))))
            (table.insert vars (math.floor val))) (opt:find "[fd]")
          (let [n (or (and (= opt :d) 8) 4)]
            (var x (stream:sub iterator (- (+ iterator n) 1)))
            (set iterator (+ iterator n))
            (when (not endianness)
              (set x (string.reverse x)))
            (var sign 1)
            (var mantissa
                 (% (string.byte x (or (and (= opt :d) 7) 3))
                    (or (and (= opt :d) 16) 128)))
            (for [i (- n 2) 1 (- 1)]
              (set mantissa (+ (* mantissa (^ 2 8)) (string.byte x i))))
            (when (> (string.byte x n) 127)
              (set sign (- 1)))
            (local exponent
                   (+ (* (% (string.byte x n) 128) (or (and (= opt :d) 16) 2))
                      (math.floor (/ (string.byte x (- n 1))
                                     (or (and (= opt :d) 16) 128)))))
            (if (= exponent 0) (table.insert vars 0.0)
                (do
                  (set mantissa (* (+ (math.ldexp mantissa
                                                  (or (and (= opt :d) (- 52))
                                                      (- 23)))
                                      1)
                                   sign))
                  (table.insert vars
                                (math.ldexp mantissa
                                            (- exponent
                                               (or (and (= opt :d) 1023) 127)))))))
          (= opt :s) (let [bytes {}]
                      (for [j iterator (stream:len) 1]
                        (when (or (= (stream:sub j j) (string.char 0))
                                  (= (stream:sub j) ""))
                          (lua :break))
                        (table.insert bytes (stream:sub j j)))
                      (local str (table.concat bytes))
                      (set iterator (+ (+ iterator (str:len)) 1))
                      (table.insert vars str)) (= opt :c)
          (let [n (: (format:sub (+ i 1)) :match "%d+")]
            (var len (tonumber n))
            (when (<= len 0)
              (set len (table.remove vars)))
            (table.insert vars (stream:sub iterator (- (+ iterator len) 1)))
            (set iterator (+ iterator len))
            (set-forcibly! i (+ i (n:len))))))
    (unpack vars)))
struct 